#=================================================================#
# Template file: sis8300bcm-acqcfg.template
# Database for the records specific to the acquisition parameters for
# the individual ACCT
# Hinko Kocevar
# October 8, 2018

record(stringout, "$(P)$(R)Name")
{
    field(VAL,  "$(NAME)")
    info(autosaveFields, "VAL")
}

record(longout, "$(P)$(R)MemoryAddress")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.MEMORY_ADDRESS")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)MemoryAddressR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.MEMORY_ADDRESS")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)NumSamples")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.NUM_SAMPLES")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)NumSamplesR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.NUM_SAMPLES")
    field(SCAN, "I/O Intr")
}

record(longout, "$(P)$(R)FractionBits")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.FRACTION_BITS")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)FractionBitsR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.FRACTION_BITS")
    field(SCAN, "I/O Intr")
}

record(ao, "$(P)$(R)Factor")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.FACTOR")
    field(PREC, "3")
    field(VAL,  "0.0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(ai, "$(P)$(R)FactorR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.FACTOR")
    field(SCAN, "I/O Intr")
    field(PREC, "3")
}

record(ao, "$(P)$(R)Offset")
{
    field(DTYP, "asynFloat64")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.OFFSET")
    field(PREC, "3")
    field(VAL,  "0.0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(ai, "$(P)$(R)OffsetR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.OFFSET")
    field(SCAN, "I/O Intr")
    field(PREC, "3")
}

record(longout, "$(P)$(R)Decimation")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.DECIMATION")
    field(DRVL, "1")
    field(DRVH, "64")
    field(VAL,  "1")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(longin, "$(P)$(R)DecimationR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.DECIMATION")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Scaling")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.SCALING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)ScalingR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.SCALING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Converting")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.CONVERTING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)ConvertingR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.CONVERTING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(bo, "$(P)$(R)Recording")
{
    field(DTYP, "asynInt32")
    field(OUT,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.RECORDING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(VAL,  "0")
    field(PINI, "YES")
    info(autosaveFields, "VAL")
}

record(bi, "$(P)$(R)RecordingR")
{
    field(DTYP, "asynInt32")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.RECORDING")
    field(ZNAM, "No")
    field(ONAM, "Yes")
    field(SCAN, "I/O Intr")
}

record(ai, "$(P)$(R)TickValueR")
{
    field(DTYP, "asynFloat64")
    field(INP,  "@asyn($(PORT),$(ADDR=0),$(TIMEOUT=1))BCM.ACQCFG.TICK_VALUE")
    field(PREC, "3")
    field(EGU,  "s")
    field(SCAN, "I/O Intr")
}
