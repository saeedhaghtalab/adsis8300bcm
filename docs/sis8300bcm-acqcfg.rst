
.. cssclass:: table-bordered table-striped table-hover
.. list-table:: Acquisition configuration record list
  :widths: 25 50 10 5 10
  :header-rows: 1

  * - Record name
    - Description
    - Record type
    - Access
    - Asyn driver info
  * - $(P)$(R)Name
    - Channel name.
    - stringout
    -
    -
  * - $(P)$(R)MemoryAddress, $(P)$(R)MemoryAddressR
    - AMC DRR memory address.
    - longout, longin
    - write, read
    - BCM.ACQCFG.MEMORY_ADDRESS
  * - $(P)$(R)NumSamples, $(P)$(R)NumSamplesR
    - Number of samples to acquire.
    - longout, longin
    - write, read
    - BCM.ACQCFG.NUM_SAMPLES
  * - $(P)$(R)FractionBits, $(P)$(R)FractionBitsR
    - Fraction bits in the sample value. Applies if the sample value is represented in floating point.
    - longout, longin
    - write, read
    - BCM.ACQCFG.FRACTION_BITS
  * - $(P)$(R)Factor, $(P)$(R)FactorR
    - Conversion factor used when scaling sample value.
    - ao, ai
    - write, read
    - BCM.ACQCFG.FACTOR
  * - $(P)$(R)Offset, $(P)$(R)OffsetR
    - Conversion offset applied when scaling sample value.
    - ao, ai
    - write, read
    - BCM.ACQCFG.OFFSET
  * - $(P)$(R)Decimation, $(P)$(R)DecimationR
    - ???
    - longout, longin
    - write, read
    - BCM.ACQCFG.DECIMATION
  * - $(P)$(R)Scaling, $(P)$(R)ScalingR
    - Scaling control.
    - bo, bi
    - write, read
    - BCM.ACQCFG.SCALING
  * - $(P)$(R)Converting, $(P)$(R)ConvertingR
    - Converting control.
    - bo, bi
    - write, read
    - BCM.ACQCFG.CONVERTING
  * - $(P)$(R)Recording, $(P)$(R)RecordingR
    - Recording control.
    - bo, bi
    - write, read
    - BCM.ACQCFG.RECORDING
  * - $(P)$(R)TickValueR
    - Sample delta value 
    - ai
    - read
    - BCM.ACQCFG.TICK_VALUE
