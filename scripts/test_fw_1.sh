#!/bin/bash
#
# setup that provides readout of 1st ADC channel
#
# NOTE: the acq tools needs to perform complete setup of the struck generic
#       parts and BCM DSP for the DSP to be functional!

[[ -n $1 ]] || { echo "Usage: $0 <device>"; exit 1; }

export PATH=$PATH:/opt/bde/R3.15.5/artifacts/adsis8300-bcm/bin/linux-x86_64

echo "fw version"
sis8300drv_reg -v $1 400

echo "fw SHA1"
sis8300drv_reg -v $1 404

# echo "use SMA clock"
# sis8300drv_reg -v $1 0x40 -w f00
echo "use TCLKA clock from EVR"
sis8300drv_reg -v $1 0x40 -w 0xA

# echo "measured freq in kHz"
sis8300drv_reg -v $1 0x412
clkkhz=$(sis8300drv_reg $1 0x412 | xargs printf "%d")
# sis8300drv_reg $1 0x412 | xargs printf "measured freq %d kHz\n"
echo "measured freq ${clkkhz} kHz"

echo "use back plane trigger line #0 (RX17)"
sis8300drv_reg -v $1 0x41D -w 3

sis8300drv_reg -v $1 413
trigper=$(sis8300drv_reg $1 0x413 | xargs printf "%d")
trigper=$(echo "1 / ${clkkhz} * ${trigper}" | bc -l | sed -e 's/\./,/')
# NOTE: if value is 0xFFFFFFFF then no trigger is arriving
printf "measured trigger period %f ms\n" ${trigper}

r=0
while [ $r -lt 24 ]; do

# setup only channel; first ADC (raw) channel
# NOTE: do not expect the readback values to be correct  (write only registers)
#       for the block 0x427 - 0x42E
# board_under_test['ACQ_CH_NO'].write(ch) 
echo "index of channel ($r)"
sis8300drv_reg -v $1 0x427 -w $r
# board_under_test['ACQ_NUM_SAMPLES'].write(no_samples) 
echo "nr of samples to acquire"
sis8300drv_reg -v $1 0x428 -w 0x20000
# board_under_test['ACQ_CH_MEM_OFFSET_ADDR'].write(ch_offset*ch+offset) 
echo "channel $r memory offset"
sis8300drv_reg -v $1 0x429 -w $(($r * 16777216))
# board_under_test['NUM_FRAC_BIT_REG'].write(0xf) 
echo "channel $r num fraction"
sis8300drv_reg -v $1 0x42A -w 0xF
# board_under_test['SCALE_FACT_REG'].write(0xc8<<15) #(0X5a<<15) + 0x2766) 
echo "channel $r scale factor"
sis8300drv_reg -v $1 0x42B -w 0x640000
# board_under_test['OFFSET_FACT_REG'].write( 0x0) 
echo "channel $r offset factor"
sis8300drv_reg -v $1 0x42C -w 0
# board_under_test['DEC_FACT_REG'].write(0x8) 
echo "channel $r decimation factor"
sis8300drv_reg -v $1 0x42D -w 0x8
# board_under_test['FP_SCALE_REC_EN_REG'].write(0x0) 
# echo "channel 0 do not bypass scaling & conversion"
# sis8300drv_reg -v $1 0x42E -w 0
echo "channel $r bypass scaling & conversion"
sis8300drv_reg -v $1 0x42E -w 7

r=$((r+1))
done

echo "channel 0 trigger source (same as 0x41D register)"
sis8300drv_reg -v $1 0x52C -w 3
