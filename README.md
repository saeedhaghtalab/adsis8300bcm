# BCM

The home of the BCM system based on [Struck Innovative Systeme](https://www.struck.de) 
[SIS8300-KU](https://www.struck.de/sis8300-ku.html) digitizer support for 
[EPICS](http://www.aps.anl.gov/epics/) 
[areaDetector](https://areadetector.github.io/master/index.html) software.

Documentation
-------------

Documentation for this module is written in reStructuredText (rst) format.
Use sphinx tool to convert to HTML.

To build the HTML output do the following in `./docs` folder:

	python3.6 -m venv $HOME/rst-docs
	source $HOME/rst-docs/bin/activate
	pip install -r requirements.txt
	pip install git+git://github.com/return42/linuxdoc.git
	make html

The result is in `_build/html` sub-folder:

	firefox _build/html/index.html &

The original build process and sphinx configuration was taken from areaDetector project and adapted for this module.

See also [reStructuredText](https://docutils.sourceforge.io/rst.html) and [Sphinx](http://www.sphinx-doc.org/en/stable/usage/restructuredtext/index.html).

Releases
--------

See [Release notes](RELEASE.md).
